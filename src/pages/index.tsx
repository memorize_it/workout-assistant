import { GetStaticProps } from 'next';
import React from 'react';
import { PageWithTitle } from './_app';

const Index = (): JSX.Element => <h1>Workout Assistant!</h1>;

export const getStaticProps: GetStaticProps<PageWithTitle> = async () => {
  return {
    props: {
      title: 'Workout Assistant - Home',
    },
  };
};

export default Index;
